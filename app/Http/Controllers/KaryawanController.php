<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
 
class KaryawanController extends Controller
{
    public function index()
    {
    	// mengambil data dari table karyawan
    	$karyawan = DB::table('karyawan')->get();
 
    	// mengirim data karyawan ke view index
    	return view('index',['karyawan' => $karyawan]);
 
    }

    public function tambah()
{
 
	// memanggil view tambah
	return view('tambah');
 
}
// method untuk CREATE data ke table karyawan
public function store(Request $request)
{
	// insert data ke table karyawan
	DB::table('karyawan')->insert([
		'id'=> $request->id,
		'nama_karyawan' => $request->nama,
		'no_karyawan' => $request->no,
		'no_telp_karyawan' => $request->notlp,
		'jabatan_karyawan' => $request->jabatan,
        'divisi_karyawan' => $request->divisi
	]);
	// alihkan halaman ke halaman karyawan
	return redirect('/');
 
}
	// method untuk edit data karyawan
public function edit($id)
{
	// mengambil data karyawan berdasarkan id yang dipilih
	$karyawan = DB::table('karyawan')->where('id',$id)->get();
	// passing data karyawan yang didapat ke view edit.blade.php
	return view('edit',['karyawan' => $karyawan]);
 
}
	// update data karyawan
public function update(Request $request)
{
	// update data karyawan
	DB::table('karyawan')->where('id', $request->id)->update([
		'nama_karyawan' => $request->nama,
		'no_karyawan' => $request->no,
		'no_telp_karyawan' => $request->notlp,
		'jabatan_karyawan' => $request->jabatan,
        'divisi_karyawan' => $request->divisi
	]);
	// alihkan halaman ke halaman karyawan
	return redirect('/');
}
// method untuk hapus data karyawan
public function hapus($id)
{
	// menghapus data karyawan berdasarkan id yang dipilih
	DB::table('karyawan')->where('id',$id)->delete();
		
	// alihkan halaman ke halaman karyawan
	return redirect('/');
}
}