<!DOCTYPE html>
<html>
<head>
	<title class>Data Karyawan</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</header>
</head>
<body>
	<div class="container" align="center">	
	<h2>Data Karyawan</h2>
<div class="table-wrapper">
    <table class="fl-table">
        <thead>
			<tr>
				<th>Nama</th>
				<th>No</th>
				<th>No Telp</th>
				<th>Jabatan</th>
				<th>Divisi</th>
				<th>Opsi</th>
			</tr>
        </thead>
        <tbody>
			@foreach($karyawan as $p)
		<tr>
			<td>{{ $p->nama_karyawan }}</td>
			<td>{{ $p->no_karyawan}}</td>
			<td>{{ $p->no_telp_karyawan }}</td>
			<td>{{ $p->jabatan_karyawan }}</td>
            <td>{{ $p->divisi_karyawan}}</td>
			<td>
				<a href="/edit/{{ $p->id }}">Edit</a>
				|
				<a href="/hapus/{{ $p->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
        <tbody>
    </table>
	
	
</div>
<a href="/tambah"> <input type="submit" value="Tambah"> </a>
</body>
</html>